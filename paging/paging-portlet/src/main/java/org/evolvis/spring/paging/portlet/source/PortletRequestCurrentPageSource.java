package org.evolvis.spring.paging.portlet.source;

import javax.portlet.PortletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.spring.paging.CurrentPageSource;
import org.evolvis.spring.paging.impl.AbstractOverridableCurrentPageSource;

/**
 * A {@link CurrentPageSource} implementation using the {@link PortletRequest} for extraction resource.
 *
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public class PortletRequestCurrentPageSource extends AbstractOverridableCurrentPageSource<PortletRequest> {

	protected final Log log = LogFactory.getLog(getClass());


	/** The {@link String page parameter}. */
	private String	pageParameter = "page";

	/**
	 * Sets the page parameter, defaults to &quot;page&quot;.
	 *
	 * @param pageParameter The new page parameter
	 */
	public void setPageParameter(String pageParameter) {
		this.pageParameter = pageParameter;
	}


	@Override
	public Integer getCurrentPage(PortletRequest request) {
		Integer currentPage = super.getCurrentPage(request);

		if (currentPage == null) {
			final String value = request.getParameter(pageParameter);

			if (value != null) {
				try {
					currentPage = Integer.parseInt(value);
				}
				catch (NumberFormatException e) {
					log.debug("Page parameter is not a number", e);
				}
			}

			if (log.isDebugEnabled()) log.debug("Extracted current page value: "+currentPage);
		}
		else {
			if (log.isDebugEnabled()) log.debug("Received overriding page value: "+currentPage);
		}

		return currentPage;
	}

	@Override
	public String getPageParameter() {
		return pageParameter ;
	}

}
