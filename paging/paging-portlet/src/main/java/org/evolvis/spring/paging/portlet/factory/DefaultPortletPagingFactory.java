package org.evolvis.spring.paging.portlet.factory;

import javax.portlet.PortletRequest;

import org.evolvis.spring.paging.CurrentPageSource;
import org.evolvis.spring.paging.ItemsPerPageSource;
import org.evolvis.spring.paging.impl.AbstractPagingFactory;
import org.evolvis.spring.paging.portlet.PortletPagingFactory;
import org.evolvis.spring.paging.portlet.source.PortletPreferencesItemsPerPageSource;
import org.evolvis.spring.paging.portlet.source.PortletSessionCurrentPageSource;


/**
 * A default {@link PortletPagingFactory} implementation.
 *
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public class DefaultPortletPagingFactory extends AbstractPagingFactory<PortletRequest> implements PortletPagingFactory {

	private CurrentPageSource<PortletRequest>	currentPageSource = new PortletSessionCurrentPageSource();

	private ItemsPerPageSource<PortletRequest>	itemsPerPageSource = new PortletPreferencesItemsPerPageSource();

	/**
	 * Sets the current page source.
	 *
	 * @param currentPageSource The new current page source
	 */
	public void setCurrentPageSource(CurrentPageSource<PortletRequest> currentPageSource) {
		this.currentPageSource = currentPageSource;
	}

	/**
	 * Sets the items per page source.
	 *
	 * @param itemsPerPageSource The new items per page source
	 */
	public void setItemsPerPageSource(ItemsPerPageSource<PortletRequest> itemsPerPageSource) {
		this.itemsPerPageSource = itemsPerPageSource;
	}


	@Override
	protected CurrentPageSource<PortletRequest> getCurrentPageSource() {
		return currentPageSource;
	}

	@Override
	protected ItemsPerPageSource<PortletRequest> getItemsPerPageSource() {
		return itemsPerPageSource;
	}

}
