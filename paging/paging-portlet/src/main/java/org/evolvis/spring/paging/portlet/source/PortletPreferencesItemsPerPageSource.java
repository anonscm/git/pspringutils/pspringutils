package org.evolvis.spring.paging.portlet.source;

import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.spring.paging.ItemsPerPageSource;
import org.evolvis.spring.paging.impl.AbstractOverridableItemsPerPageSource;

/**
 * An {@link ItemsPerPageSource} using {@link PortletPreferences} for extraction resource.
 *
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public class PortletPreferencesItemsPerPageSource extends AbstractOverridableItemsPerPageSource<PortletRequest> {

	protected final Log log = LogFactory.getLog(getClass());


	/** The {@link String items per page preferences key}. */
	private String	itemsPerPagePreferencesKey = "paging.portlet.itemsPerPage";

	/**
	 * Sets the items per page {@link PortletPreferences} key, defaults to &quot;paging.portlet.itemsPerPage&quot;.
	 *
	 * @param itemsPerPagePreferencesKey The new items per page preferences key
	 */
	public void setItemsPerPagePreferencesKey(String itemsPerPagePreferencesKey) {
		this.itemsPerPagePreferencesKey = itemsPerPagePreferencesKey;
	}


	@Override
	public Long getItemsPerPage(PortletRequest request) {
		Long itemsPerPage = super.getItemsPerPage(request);

		if (itemsPerPage == null) {
			final PortletPreferences prefs = request.getPreferences();

			final String value = prefs.getValue(itemsPerPagePreferencesKey, null);

			if (value != null) {
				try {
					itemsPerPage = Long.parseLong(value);
				}
				catch (NumberFormatException e) {
					log.warn("Items per page parameter is not a number", e);
				}
			}

			if (log.isDebugEnabled()) log.debug("Extracted ipp value: "+itemsPerPage);
		}
		else {
			if (log.isDebugEnabled()) log.debug("Received overriding ipp value: "+itemsPerPage);
		}

		return itemsPerPage;
	}

}
