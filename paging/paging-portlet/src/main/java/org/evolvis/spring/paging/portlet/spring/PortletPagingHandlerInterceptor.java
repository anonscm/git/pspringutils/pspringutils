package org.evolvis.spring.paging.portlet.spring;

import static org.evolvis.spring.paging.portlet.spring.PortletPublishUtils.publishPaging;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.evolvis.spring.paging.Paging;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.portlet.HandlerInterceptor;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.handler.HandlerInterceptorAdapter;

/**
 * A simple {@link HandlerInterceptor} publishing the {@link Paging} object into the {@link Model} if available.
 *
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public class PortletPagingHandlerInterceptor extends HandlerInterceptorAdapter {

	@Override
	public void postHandleRender(RenderRequest request, RenderResponse response, Object handler, ModelAndView modelAndView)
	throws Exception {
		publishPaging(request, getModel(modelAndView));
	}

	@Override
	public void postHandleResource(ResourceRequest request, ResourceResponse response, Object handler, ModelAndView modelAndView)
	throws Exception {
		publishPaging(request, getModel(modelAndView));
	}


	private ModelMap getModel(ModelAndView modelAndView) {
		if (modelAndView == null) {
			throw new IllegalStateException("No model received from handler, use or return a Model in your handler");
		}

		return modelAndView.getModelMap();
	}

}
