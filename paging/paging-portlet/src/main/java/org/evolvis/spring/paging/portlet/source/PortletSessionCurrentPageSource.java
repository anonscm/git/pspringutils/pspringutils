package org.evolvis.spring.paging.portlet.source;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;

import org.evolvis.spring.paging.CurrentPageSource;

/**
 * A {@link CurrentPageSource} extension of {@link PortletRequestCurrentPageSource}
 * using the {@link PortletSession} for persistence.
 *
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public class PortletSessionCurrentPageSource extends PortletRequestCurrentPageSource {

	/** The {@link String session attribute name}. */
	private String	sessionAttributeName = "paging.portlet.currentPage";

	/** The session scope. */
	private int	sessionScope = PortletSession.PORTLET_SCOPE;

	/**
	 * Sets the session attribute name, defaults to &quot;paging.portlet.currentPage&quot;.
	 *
	 * @param sessionAttributeName The new session attribute name
	 */
	public void setSessionAttributeName(String sessionAttributeName) {
		this.sessionAttributeName = sessionAttributeName;
	}

	/**
	 * Sets the session scope, defaults to {@link PortletSession#PORTLET_SCOPE}.
	 *
	 * @param sessionScope The new session scope
	 */
	public void setSessionScope(int sessionScope) {
		this.sessionScope = sessionScope;
	}


	/**
	 * {@inheritDoc}<br /><br />
	 * Additionally the current page is persisted in the {@link PortletSession}
	 * and retrieved if request does not override.
	 */
	@Override
	public Integer getCurrentPage(PortletRequest request) {
		Integer currentPage = super.getCurrentPage(request);

		final PortletSession session = request.getPortletSession();

		if (currentPage == null) {
			// get current page from previously stored session
			Object obj = session.getAttribute(sessionAttributeName, sessionScope);

			if (obj != null) {
				if (obj instanceof Integer) {
					// Found current page
					currentPage = (Integer) obj;
				}
				else {
					if (log.isErrorEnabled()) {
						log.error("Unknown object found in session attribute " + sessionAttributeName
								+ " of type " + obj.getClass().getCanonicalName());
					}
				}
			}
			// Else no page previously stored in session

			if (log.isDebugEnabled()) log.debug("Extracted current page value: "+currentPage);
		}
		else {
			// Store current page from request into session
			if (log.isDebugEnabled()) log.debug("Persisting current page value from request: "+currentPage);

			session.setAttribute(sessionAttributeName, currentPage, sessionScope);
		}

		return currentPage;
	}

}
