package org.evolvis.spring.paging.portlet;

import javax.portlet.PortletRequest;

import org.evolvis.spring.paging.PagingFactory;


/**
 * A {@link PagingFactory} using {@link PortletRequest PortletRequests} for extraction resource.
 *
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public interface PortletPagingFactory extends PagingFactory<PortletRequest> {

}
