package org.evolvis.spring.paging.portlet.source;

import javax.portlet.PortletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.spring.paging.ItemsPerPageSource;
import org.evolvis.spring.paging.impl.AbstractOverridableItemsPerPageSource;

/**
 * An {@link ItemsPerPageSource} using the {@link PortletRequest} for extraction resource.
 *
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public class PortletRequestItemsPerPageSource extends AbstractOverridableItemsPerPageSource<PortletRequest> {

	protected final Log log = LogFactory.getLog(getClass());


	/** The {@link String items per page parameter}. */
	private String	itemsPerPageParameter = "itemsPerPage";

	/**
	 * Sets the items per page parameter, defaults to &quot;itemsPerPage&quot;.
	 *
	 * @param itemsPerPageParameter The new items per page parameter
	 */
	public void setItemsPerPageParameter(String itemsPerPageParameter) {
		this.itemsPerPageParameter = itemsPerPageParameter;
	}


	@Override
	public Long getItemsPerPage(PortletRequest request) {
		Long itemsPerPage = super.getItemsPerPage(request);

		if (itemsPerPage == null) {
			final String value = request.getParameter(itemsPerPageParameter);


			if (value != null) {
				try {
					itemsPerPage = Long.parseLong(value);
				}
				catch (NumberFormatException e) {
					log.debug("Items per page parameter is not a number", e);
				}
			}

			if (log.isDebugEnabled()) log.debug("Extracted ipp value: "+itemsPerPage);
		}
		else {
			if (log.isDebugEnabled()) log.debug("Received overriding ipp value: "+itemsPerPage);
		}

		return itemsPerPage;
	}

}
