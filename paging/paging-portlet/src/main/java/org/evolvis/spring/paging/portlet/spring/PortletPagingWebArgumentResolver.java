package org.evolvis.spring.paging.portlet.spring;

import javax.portlet.PortletRequest;

import org.evolvis.spring.paging.Paging;
import org.evolvis.spring.paging.spring.PagingWebArgumentResolver;
import org.springframework.web.context.request.NativeWebRequest;

/**
 * The portlet implementation of {@link PagingWebArgumentResolver}.
 *
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public class PortletPagingWebArgumentResolver extends PagingWebArgumentResolver {

	@Override
	protected void storePagingInRequest(Paging paging, String modelAttr, NativeWebRequest webRequest) {
		PortletPublishUtils.storePaging(paging, modelAttr, webRequest.getNativeRequest(PortletRequest.class));
	}

}
