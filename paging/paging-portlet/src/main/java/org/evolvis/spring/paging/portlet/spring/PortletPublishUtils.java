package org.evolvis.spring.paging.portlet.spring;

import java.util.Collections;

import javax.portlet.PortletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.spring.paging.Paging;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;

/**
 * Utils class for {@link Paging} publishing.
 *
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
class PortletPublishUtils {

	private static final Log log = LogFactory.getLog(PortletPublishUtils.class);


	private static final String REQUEST_ATTRIBUTE_PREFIX =
		PortletPagingWebArgumentResolver.class.getPackage().getName() + ".";

	private static final String REQUEST_ATTRIBUTE_ISSET = REQUEST_ATTRIBUTE_PREFIX + "set";

	private static final String REQUEST_ATTRIBUTE_PAGINGS_PREFIX = REQUEST_ATTRIBUTE_PREFIX + "pagings.";


	/**
	 * Static-only class.
	 */
	private PortletPublishUtils() {
	}


	/**
	 * Stores the {@link Paging} object in the {@link PortletRequest}.
	 *
	 * @param paging The paging
	 * @param modelAttr The model attribute name
	 * @param request The request
	 */
	public static void storePaging(Paging paging, String modelAttr, PortletRequest request) {
		request.setAttribute(REQUEST_ATTRIBUTE_ISSET, true);

		request.setAttribute(REQUEST_ATTRIBUTE_PAGINGS_PREFIX + modelAttr, paging);

		if (log.isTraceEnabled()) {
			log.trace("Stored paging with model attribute "+modelAttr);
		}
	}


	/**
	 * Publishes the {@link Paging} object into the {@link ModelMap} if set in the {@link PortletRequest}.
	 *
	 * @param request The request
	 * @param modelMap The model map
	 */
	public static void publishPaging(PortletRequest request, ModelMap modelMap) {
		if (request.getAttribute(REQUEST_ATTRIBUTE_ISSET) != null) {
			for (final String attr : Collections.list(request.getAttributeNames())) {
				if (attr.startsWith(REQUEST_ATTRIBUTE_PAGINGS_PREFIX)) {
					final String modelName = attr.substring(REQUEST_ATTRIBUTE_PAGINGS_PREFIX.length());

					if (StringUtils.hasText(modelName)) {
						// Force cast for fast-fail
						modelMap.addAttribute(modelName, request.getAttribute(attr));

						if (log.isTraceEnabled()) {
							log.trace("Published paging with name "+modelName);
						}
					}
					else {
						log.trace("Empty model name detected");
					}
				}
			}
		}
		else {
			log.trace("No pagings in request");
		}
	}

}
