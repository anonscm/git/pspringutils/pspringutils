package org.evolvis.spring.paging.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.spring.paging.CurrentPageSource;
import org.evolvis.spring.paging.ItemsPerPageSource;
import org.evolvis.spring.paging.Paging;
import org.evolvis.spring.paging.PagingFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;

/**
 * An abstract implementation of the {@link PagingFactory}.
 *
 * @param <T> The extraction resource type
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public abstract class AbstractPagingFactory<T> implements PagingFactory<T>, BeanFactoryAware {

	protected final Log log = LogFactory.getLog(getClass());

	private BeanFactory beanFactory;

	private String pagingBeanName;

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}

	/**
	 * Sets the name of the configured {@link DefaultPaging} bean.<br />
	 * Defaults to using a bean of type {@link DefaultPaging}.<br /><br />
	 * If nothing is configured then this factory will create a new object on its own.
	 */
	public void setPagingBeanName(String pagingBeanName) {
		this.pagingBeanName = pagingBeanName;
	}


	@Override
	public Paging getPaging(T source, Long totalDataItems) {
		DefaultPaging paging;

		try {
			if (pagingBeanName == null) {
				paging = beanFactory.getBean(DefaultPaging.class);
			}
			else {
				paging = beanFactory.getBean(pagingBeanName, DefaultPaging.class);
			}
		}
		catch (BeansException e) {
			log.debug("Cannot find DefaultPaging bean, using direct instance", e);

			paging = new DefaultPaging();
		}


		// Completing paging prototype
		Integer currentPage = getCurrentPageSource().getCurrentPage(source);
		if (currentPage != null) paging.setCurrentPage(currentPage);

		Long itemsPerPage = getItemsPerPageSource().getItemsPerPage(source);
		if (itemsPerPage != null) paging.setItemsPerPage(itemsPerPage);

		String pageParameter = getCurrentPageSource().getPageParameter();
		if (pageParameter != null) paging.setPageParameter(pageParameter);

		if (totalDataItems != null) paging.setTotalItems(totalDataItems);

		return paging;
	}

	@Override
	public Paging getPaging(T source) {
		return getPaging(source, null);
	}


	/**
	 * Gets the current page source.
	 *
	 * @return The current page source
	 */
	protected abstract CurrentPageSource<T> getCurrentPageSource();

	/**
	 * Gets the items per page source.
	 *
	 * @return The items per page source
	 */
	protected abstract ItemsPerPageSource<T> getItemsPerPageSource();

}
