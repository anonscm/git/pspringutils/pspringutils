package org.evolvis.spring.paging.spring;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.spring.paging.Paging;
import org.evolvis.spring.paging.PagingFactory;
import org.evolvis.spring.paging.spring.annotation.Pager;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.context.request.NativeWebRequest;

/**
 * A {@link WebArgumentResolver} for {@link Paging} objects.
 *
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public abstract class PagingWebArgumentResolver implements WebArgumentResolver, BeanFactoryAware {

	protected final Log log = LogFactory.getLog(getClass());

	private BeanFactory	beanFactory;

	private PagingFactory<?> defaultPagingFactory;

	/**
	 * Sets the default paging factory.
	 *
	 * @param defaultPagingFactory The new default paging factory
	 */
	public void setDefaultPagingFactory(PagingFactory<?> defaultPagingFactory) {
		this.defaultPagingFactory = defaultPagingFactory;
	}


	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}


	@Override
	public Object resolveArgument(MethodParameter methodParameter, NativeWebRequest webRequest) throws Exception {
		final Pager pager;

		if (methodParameter.getParameterType().equals(Paging.class) &&
				(pager = findAnnotation(methodParameter)) != null) {
			final PagingFactory<?> factory = getAnnotatedFactory(pager);

			try {
				@SuppressWarnings("unchecked")
				final PagingFactory<Object> objFactory = (PagingFactory<Object>) factory;

				final Paging paging = objFactory.getPaging(webRequest.getNativeRequest());

				storePaging(paging, pager, webRequest);

				return paging;
			}
			catch (ClassCastException e) {
				log.error("Cannot use native request object for this paging factory", e);
			}
		}

		return WebArgumentResolver.UNRESOLVED;
	}


	/**
	 * Stores the paging for later publishing in view model.
	 *
	 * @param paging The paging
	 * @param pager The pager
	 * @param webRequest The web request
	 */
	private void storePaging(Paging paging, Pager pager, NativeWebRequest webRequest) {
		final String modelAttr = pager.model();

		if (StringUtils.hasText(modelAttr)) {
			storePagingInRequest(paging, modelAttr, webRequest);
		}
	}


	/**
	 * Stores the paging object in the web request.
	 *
	 * @param paging The paging
	 * @param modelAttr The model attribute name
	 * @param webRequest The web request
	 */
	protected abstract void storePagingInRequest(Paging paging, String modelAttr, NativeWebRequest webRequest);




	/**
	 * Finds the {@link Pager} annotation for the method parameter.
	 *
	 * @param methodParameter The method parameter
	 * @return The pager annotation
	 */
	private Pager findAnnotation(MethodParameter methodParameter) {
		// TODO Use caching for speed-up
		// Look for parameter annotation
		Pager anno = methodParameter.getParameterAnnotation(Pager.class);

		if (anno == null) {
			// Fall back to method annotation
			anno = methodParameter.getMethodAnnotation(Pager.class);
		}

		if (anno == null) {
			// Fall back to type annotation
			anno = AnnotationUtils.findAnnotation(methodParameter.getDeclaringClass(), Pager.class);
		}

		return anno;
	}


	/**
	 * Gets the factory configured by the annotation.
	 *
	 * @param annotation The annotation
	 * @return The factory
	 */
	private PagingFactory<?> getAnnotatedFactory(Pager annotation) {
		final String name = annotation.factory();
		final PagingFactory<?> factory;

		if (StringUtils.hasText(name)) {
			factory = beanFactory.getBean(name, PagingFactory.class);
		}
		else if (defaultPagingFactory != null) {
			factory = defaultPagingFactory;
		}
		else {
			factory = beanFactory.getBean(PagingFactory.class);
		}

		if (log.isDebugEnabled()) {
			log.debug("Resolved PagingFactory for name "+name+" to "+factory);
		}

		return factory;
	}

}
