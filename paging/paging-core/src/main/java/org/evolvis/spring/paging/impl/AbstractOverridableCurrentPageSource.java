package org.evolvis.spring.paging.impl;

import org.evolvis.spring.paging.CurrentPageSource;

/**
 * An abstract {@link CurrentPageSource} implementation using another {@link CurrentPageSource} for override.
 *
 * @param <T> The extraction resource type
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public abstract class AbstractOverridableCurrentPageSource<T> implements CurrentPageSource<T> {

	/** The {@link CurrentPageSource overriding source}. */
	protected CurrentPageSource<T> overridingSource;

	/**
	 * Sets the overriding source.
	 *
	 * @param overridingSource The new overriding source
	 */
	public void setOverridingSource(CurrentPageSource<T> overridingSource) {
		this.overridingSource = overridingSource;
	}


	/**
	 * {@inheritDoc}<br /><br />
	 * On extending this class usually you should override this method
	 * and call this super method first. If it returns a non-<code>null</code> result
	 * then return it, otherwise use your implementation.
	 */
	@Override
	public Integer getCurrentPage(T source) {
		if (overridingSource == null) {
			return null;
		}
		else {
			return overridingSource.getCurrentPage(source);
		}
	}

}
