package org.evolvis.spring.paging.impl;

import static java.lang.Math.ceil;

import org.evolvis.spring.paging.Paging;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;


/**
 * A paging bean object for easy parameter paging.
 *
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DefaultPaging implements Paging {

	private static final long	serialVersionUID	= -5178141542896554437L;


	/** The Constant for the default items per page. */
	static final long DEFAULT_ITEMS_PER_PAGE = 30L;


	/** The first page number to use, pages then enumerate to first, first + 1, ... */
	private int firstPage = 1;

	/** The first index used for index calculations. */
	private long firstIndex = 0L;

	/** The {@link String page parameter} usable when constructing urls. */
	private String pageParameter = "page";


	/** The current page number in this paging object. */
	private int currentPage = firstPage;

	/** The total number of items. */
	private long totalItems;

	/** The number of items per page. */
	private long itemsPerPage = DEFAULT_ITEMS_PER_PAGE;



	/**
	 * Sets the first page number to use, defaults to 1.<br /><br />
	 * <b>Note</b> that if the current page number is lower then it will be reset to the new first page number.
	 *
	 * @param firstPage
	 */
	public void setFirstPage(int firstPage) {
		this.firstPage = firstPage;
		adaptCurrentPage();
	}

	/**
	 * Assures that the current page is correct according to the first page.
	 */
	protected void adaptCurrentPage() {
		if (getCurrentPage() < getFirstPage()) {
			setCurrentPage(getFirstPage());
		}
	}

	/**
	 * Sets the first index used for index calculations, defaults to 0.
	 *
	 * @param firstIndex
	 */
	public void setFirstIndex(long firstIndex) {
		this.firstIndex = firstIndex;
	}



	@Override
	public int getFirstPage() {
		return firstPage;
	}

	@Override
	public long getFirstIndex() {
		return firstIndex;
	}

	@Override
	public int getCurrentPage() {
		return currentPage;
	}

	/**
	 * Sets the current page number, defaults to first page number.<br /><br />
	 * <b>Note</b> that a number lower than the first page number will be reset to the first page number.
	 *
	 * @param currentPage The new current page number
	 */
	void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
		adaptCurrentPage();
	}

	@Override
	public long getItemsPerPage() {
		return itemsPerPage;
	}

	/**
	 * Sets the number of items per page.
	 *
	 * @param itemsPerPage The new number of items per page
	 * @throws IllegalArgumentException if value is not positive
	 */
	public void setItemsPerPage(long itemsPerPage) {
		if (itemsPerPage <= 0L) throw new IllegalArgumentException("Illegal number for items per page: "+itemsPerPage);

		this.itemsPerPage = itemsPerPage;
	}

	@Override
	public String getPageParameter() {
		return pageParameter;
	}

	/**
	 * Sets the page parameter.
	 *
	 * @param pageParameter The new page parameter
	 * @throws IllegalArgumentException if parameter has no text
	 */
	void setPageParameter(String pageParameter) {
		if (!StringUtils.hasText(pageParameter)) throw new IllegalArgumentException("Page parameter required");

		this.pageParameter = pageParameter;
	}


	@Override
	public long getTotalItems() {
		return totalItems;
	}

	@Override
	public void setTotalItems(long total) {
		if (total < 0L) throw new IllegalArgumentException("Illegal number of total items: "+total);

		this.totalItems = total;
	}

	/**
	 * Gets the number of total items or throws exception if items are empty.
	 *
	 * @return The number of total items
	 * @throws IllegalStateException if total items are empty
	 */
	protected long getCheckedTotalItems() {
		if (isResultEmpty()) {
			throw new IllegalStateException("No items present, check total items first");
		}

		return getTotalItems();
	}

	@Override
	public boolean isResultEmpty() {
		return getTotalItems() == 0L;
	}

	@Override
	public int getLastPage() {
		return ((int) (ceil(getCheckedTotalItems() / ((double) getItemsPerPage())))) + getFirstPage() - 1;
	}

	@Override
	public long getStartIndex() {
		return ((getCurrentPage() - getFirstPage())) * getItemsPerPage() + getFirstIndex();
	}

	@Override
	public long getEndIndex() {
		return getStartIndex() + getItemsPerPage();
	}

	@Override
	public long getLastIndex() {
		return getCheckedTotalItems() + getFirstIndex() - 1;
	}

	@Override
	public long getOffset() {
		return getStartIndex();
	}

	@Override
	public long getLimit() {
		return getItemsPerPage();
	}

	@Override
	public boolean isTotalExceeded() {
		return getStartIndex() - getFirstIndex() >= getTotalItems();
	}

}
