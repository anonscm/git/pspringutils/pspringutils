package org.evolvis.spring.paging.spring.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.evolvis.spring.paging.Paging;
import org.evolvis.spring.paging.PagingFactory;
import org.springframework.ui.Model;

/**
 * Annotation for configuring paging.
 *
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface Pager {

	/**
	 * The {@link PagingFactory} name or empty to use the default factory.
	 */
	String factory() default "";

	/**
	 * The {@link Model} attribute name where to auto-publish the {@link Paging} object or empty to omit publishment.
	 */
	String model() default "";

}
