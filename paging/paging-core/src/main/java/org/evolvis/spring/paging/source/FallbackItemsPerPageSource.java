package org.evolvis.spring.paging.source;

import org.evolvis.spring.paging.ItemsPerPageSource;
import org.evolvis.spring.paging.impl.AbstractOverridableItemsPerPageSource;

/**
 * A simple {@link ItemsPerPageSource} never returning a value.
 *
 * @param <T> The unused extraction resource type
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public class FallbackItemsPerPageSource<T> extends AbstractOverridableItemsPerPageSource<T> {

	@Override
	public Long getItemsPerPage(T source) {
		return null;
	}

}
