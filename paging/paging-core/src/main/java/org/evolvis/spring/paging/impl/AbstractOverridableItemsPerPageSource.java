package org.evolvis.spring.paging.impl;

import org.evolvis.spring.paging.ItemsPerPageSource;

/**
 * An abstract {@link ItemsPerPageSource} implementation using another {@link ItemsPerPageSource} for override.
 *
 * @param <T> The extraction resource type
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public abstract class AbstractOverridableItemsPerPageSource<T> implements ItemsPerPageSource<T> {

	/** The {@link ItemsPerPageSource overriding source}. */
	protected ItemsPerPageSource<T> overridingSource;

	/**
	 * Sets the overriding source.
	 *
	 * @param overridingSource The new overriding source
	 */
	public void setOverridingSource(ItemsPerPageSource<T> overridingSource) {
		this.overridingSource = overridingSource;
	}


	/**
	 * {@inheritDoc}<br /><br />
	 * On extending this class usually you should override this method
	 * and call this super method first. If it returns a non-<code>null</code> result
	 * then return it, otherwise use your implementation.
	 */
	@Override
	public Long getItemsPerPage(T source) {
		if (overridingSource == null) {
			return null;
		}
		else {
			return overridingSource.getItemsPerPage(source);
		}
	}

}
