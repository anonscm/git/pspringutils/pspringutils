package org.evolvis.spring.paging.impl;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.evolvis.spring.paging.CurrentPageSource;
import org.evolvis.spring.paging.ItemsPerPageSource;
import org.evolvis.spring.paging.Paging;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;

@RunWith(JMock.class)
public class AbstractPagingFactoryTest {

	private class TestPagingFactory extends AbstractPagingFactory<Integer> {

		@Override
		protected CurrentPageSource<Integer> getCurrentPageSource() {
			return currentPageSource;
		}

		@Override
		protected ItemsPerPageSource<Integer> getItemsPerPageSource() {
			return itemsPerPageSource;
		}

	}

	private AbstractPagingFactory<Integer> factory;

	private Mockery mockery;

	private CurrentPageSource<Integer>	currentPageSource;
	private ItemsPerPageSource<Integer>	itemsPerPageSource;
	private BeanFactory beanFactory;
	private final String beanName = "testfoo";


	@Before
	@SuppressWarnings("unchecked")
	public void setup() {
		mockery = new JUnit4Mockery();

		factory = new TestPagingFactory();

		currentPageSource = mockery.mock(CurrentPageSource.class);
		itemsPerPageSource = mockery.mock(ItemsPerPageSource.class);
		beanFactory = mockery.mock(BeanFactory.class);

		factory.setBeanFactory(beanFactory);
		factory.setPagingBeanName(beanName);
	}


	@Test
	public void testGetPaging1() throws Exception {
		factory.setPagingBeanName(null);

		mockery.checking(new Expectations() {{
			oneOf(beanFactory).getBean(DefaultPaging.class);
			will(throwException(new NoSuchBeanDefinitionException("")));

			oneOf(currentPageSource).getCurrentPage(4);
			will(returnValue(36));

			oneOf(currentPageSource).getPageParameter();
			will(returnValue("foobar"));

			oneOf(itemsPerPageSource).getItemsPerPage(4);
			will(returnValue(71L));
		}});

		Paging paging = factory.getPaging(4);

		assertEquals(36, paging.getCurrentPage());
		assertEquals("foobar", paging.getPageParameter());
		assertEquals(71L, paging.getItemsPerPage());
		assertEquals(0L, paging.getTotalItems());
	}

	@Test
	public void testGetPaging2() throws Exception {
		factory.setPagingBeanName(null);

		mockery.checking(new Expectations() {{
			oneOf(beanFactory).getBean(DefaultPaging.class);
			will(returnValue(new DefaultPaging()));

			oneOf(currentPageSource).getCurrentPage(4);
			will(returnValue(36));

			oneOf(currentPageSource).getPageParameter();
			will(returnValue("foobar"));

			oneOf(itemsPerPageSource).getItemsPerPage(4);
			will(returnValue(71L));
		}});

		Paging paging = factory.getPaging(4);

		assertEquals(36, paging.getCurrentPage());
		assertEquals("foobar", paging.getPageParameter());
		assertEquals(71L, paging.getItemsPerPage());
		assertEquals(0L, paging.getTotalItems());
	}

	@Test
	public void testGetPaging3() throws Exception {
		mockery.checking(new Expectations() {{
			oneOf(beanFactory).getBean(beanName, DefaultPaging.class);
			will(throwException(new NoSuchBeanDefinitionException("")));

			oneOf(currentPageSource).getCurrentPage(4);
			will(returnValue(36));

			oneOf(currentPageSource).getPageParameter();
			will(returnValue("foobar"));

			oneOf(itemsPerPageSource).getItemsPerPage(4);
			will(returnValue(71L));
		}});

		Paging paging = factory.getPaging(4);

		assertEquals(36, paging.getCurrentPage());
		assertEquals("foobar", paging.getPageParameter());
		assertEquals(71L, paging.getItemsPerPage());
		assertEquals(0L, paging.getTotalItems());
	}

	@Test
	public void testGetPaging4() throws Exception {
		mockery.checking(new Expectations() {{
			oneOf(beanFactory).getBean(beanName, DefaultPaging.class);
			will(returnValue(new DefaultPaging()));

			oneOf(currentPageSource).getCurrentPage(4);
			will(returnValue(36));

			oneOf(currentPageSource).getPageParameter();
			will(returnValue("foobar"));

			oneOf(itemsPerPageSource).getItemsPerPage(4);
			will(returnValue(71L));
		}});

		Paging paging = factory.getPaging(4);

		assertEquals(36, paging.getCurrentPage());
		assertEquals("foobar", paging.getPageParameter());
		assertEquals(71L, paging.getItemsPerPage());
		assertEquals(0L, paging.getTotalItems());
	}

	@Test
	public void testGetPaging5() throws Exception {
		mockery.checking(new Expectations() {{
			oneOf(beanFactory).getBean(beanName, DefaultPaging.class);
			will(returnValue(new DefaultPaging()));

			oneOf(currentPageSource).getCurrentPage(4);
			will(returnValue(null));

			oneOf(currentPageSource).getPageParameter();
			will(returnValue("foobar"));

			oneOf(itemsPerPageSource).getItemsPerPage(4);
			will(returnValue(71L));
		}});

		Paging paging = factory.getPaging(4);

		assertEquals(1, paging.getCurrentPage());
		assertEquals("foobar", paging.getPageParameter());
		assertEquals(71L, paging.getItemsPerPage());
		assertEquals(0L, paging.getTotalItems());
	}

	@Test
	public void testGetPaging6() throws Exception {
		mockery.checking(new Expectations() {{
			oneOf(beanFactory).getBean(beanName, DefaultPaging.class);
			will(returnValue(new DefaultPaging()));

			oneOf(currentPageSource).getCurrentPage(4);
			will(returnValue(null));

			oneOf(currentPageSource).getPageParameter();
			will(returnValue("foobar"));

			oneOf(itemsPerPageSource).getItemsPerPage(4);
			will(returnValue(null));
		}});

		Paging paging = factory.getPaging(4);

		assertEquals(1, paging.getCurrentPage());
		assertEquals("foobar", paging.getPageParameter());
		assertTrue(paging.getItemsPerPage() > 0L);
		assertEquals(0L, paging.getTotalItems());
	}

	@Test
	public void testGetPagingWithTotal() throws Exception {
		mockery.checking(new Expectations() {{
			oneOf(beanFactory).getBean(beanName, DefaultPaging.class);
			will(returnValue(new DefaultPaging()));

			oneOf(currentPageSource).getCurrentPage(4);
			will(returnValue(36));

			oneOf(currentPageSource).getPageParameter();
			will(returnValue("foobar"));

			oneOf(itemsPerPageSource).getItemsPerPage(4);
			will(returnValue(71L));
		}});

		Paging paging = factory.getPaging(4, 1393L);

		assertEquals(36, paging.getCurrentPage());
		assertEquals("foobar", paging.getPageParameter());
		assertEquals(71L, paging.getItemsPerPage());
		assertEquals(1393L, paging.getTotalItems());
	}

}
