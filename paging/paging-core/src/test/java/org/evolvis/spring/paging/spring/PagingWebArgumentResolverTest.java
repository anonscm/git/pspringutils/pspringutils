package org.evolvis.spring.paging.spring;

import static org.junit.Assert.assertSame;

import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicReference;

import org.evolvis.spring.paging.Paging;
import org.evolvis.spring.paging.PagingFactory;
import org.evolvis.spring.paging.spring.annotation.Pager;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.core.MethodParameter;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.ReflectionUtils.MethodCallback;
import org.springframework.util.ReflectionUtils.MethodFilter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.context.request.NativeWebRequest;

@RunWith(JMock.class)
public class PagingWebArgumentResolverTest {

	private static interface Storer {

		public void store(Paging paging, String modelAttr, NativeWebRequest webRequest);

	}

	private class TestResolver extends PagingWebArgumentResolver {

		@Override
		protected void storePagingInRequest(Paging paging, String modelAttr, NativeWebRequest webRequest) {
			storer.store(paging, modelAttr, webRequest);
		}

	}

	@SuppressWarnings("unused")
	private static class TestClass1 {

		public void testMethod1(
				@Pager Paging annotatedPaging,
				String nonPaging,
				Paging paging) { }

		public void testMethod2(
				@Pager(factory="myFactory") Paging annotatedPaging) { }

		public void testMethod3(
				@Pager(factory="myFactory", model="myModelKey") Paging annotatedPaging) { }

		public void testMethod4(
				@Pager(model="myModelKey") Paging annotatedPaging) { }

		@Pager(factory="methodFactory", model="methodModel")
		public void testMethod11(
				@Pager Paging annotatedPaging) { }

		@Pager(factory="methodFactory", model="methodModel")
		public void testMethod12(
				Paging annotatedPaging) { }

		@Pager(factory="methodFactory", model="methodModel")
		public void testMethod13(
				@Pager(factory="myFactory", model="myModelKey") Paging annotatedPaging) { }

	}

	@SuppressWarnings("unused")
	@Pager(factory="classFactory", model="classModel")
	private static class TestClass2 {

		public void testMethod1(
				@Pager Paging annotatedPaging,
				String nonPaging,
				Paging paging) { }

		public void testMethod2(
				@Pager(factory="myFactory") Paging annotatedPaging) { }

		public void testMethod3(
				@Pager(factory="myFactory", model="myModelKey") Paging annotatedPaging) { }

		public void testMethod4(
				@Pager(model="myModelKey") Paging annotatedPaging) { }

		@Pager(factory="methodFactory", model="methodModel")
		public void testMethod11(
				@Pager Paging annotatedPaging) { }

		@Pager(factory="methodFactory", model="methodModel")
		public void testMethod12(
				Paging annotatedPaging) { }

		@Pager(factory="methodFactory", model="methodModel")
		public void testMethod13(
				@Pager(factory="myFactory", model="myModelKey") Paging annotatedPaging) { }

		public void testMethod14(
				Paging annotatedPaging) { }

	}


	private Mockery mockery;

	private TestResolver resolver;

	private Storer storer;
	private BeanFactory beanFactory;


	@Before
	public void setup() {
		mockery = new JUnit4Mockery();

		resolver = new TestResolver();

		storer = mockery.mock(Storer.class);
		beanFactory = mockery.mock(BeanFactory.class);

		resolver.setBeanFactory(beanFactory);
	}


	@Test
	public void testResolve_1_1_0() throws Exception {
		doTestResolve(1, 1, 0, null);
	}

	@Test
	public void testResolve_1_1_1() throws Exception {
		doTestResolve(1, 1, 1, false, null);
	}

	@Test
	public void testResolve_1_1_2() throws Exception {
		doTestResolve(1, 1, 1, false, null);
	}

	@Test
	public void testResolve_1_2() throws Exception {
		doTestResolve(1, 2, "myFactory");
	}

	@Test
	public void testResolve_1_3() throws Exception {
		doTestResolve(1, 3, "myFactory", "myModelKey");
	}

	@Test
	public void testResolve_1_4() throws Exception {
		doTestResolve(1, 4, null, "myModelKey");
	}

	@Test
	public void testResolve_1_11() throws Exception {
		doTestResolve(1, 11, null);
	}

	@Test
	public void testResolve_1_12() throws Exception {
		doTestResolve(1, 12, "methodFactory", "methodModel");
	}

	@Test
	public void testResolve_1_13() throws Exception {
		doTestResolve(1, 13, "myFactory", "myModelKey");
	}


	@Test
	public void testResolve_2_1_0() throws Exception {
		doTestResolve(2, 1, 0, null);
	}

	@Test
	public void testResolve_2_1_1() throws Exception {
		doTestResolve(2, 1, 1, false, null);
	}

	@Test
	public void testResolve_2_1_2() throws Exception {
		doTestResolve(2, 1, 1, false, null);
	}

	@Test
	public void testResolve_2_2() throws Exception {
		doTestResolve(2, 2, "myFactory");
	}

	@Test
	public void testResolve_2_3() throws Exception {
		doTestResolve(2, 3, "myFactory", "myModelKey");
	}

	@Test
	public void testResolve_2_4() throws Exception {
		doTestResolve(2, 4, null, "myModelKey");
	}

	@Test
	public void testResolve_2_11() throws Exception {
		doTestResolve(2, 11, null);
	}

	@Test
	public void testResolve_2_12() throws Exception {
		doTestResolve(2, 12, "methodFactory", "methodModel");
	}

	@Test
	public void testResolve_2_13() throws Exception {
		doTestResolve(2, 13, "myFactory", "myModelKey");
	}

	@Test
	public void testResolve_2_14() throws Exception {
		doTestResolve(2, 14, "classFactory", "classModel");
	}


	private void doTestResolve(int cl, int mi, int pi, final boolean resolved, final String name, final String store)
	throws Exception {
		Class<?> clazz = ClassUtils.forName(getClass().getName()+"$TestClass"+cl, getClass().getClassLoader());
		Method method = findMethod(clazz, "testMethod"+mi);
		MethodParameter param = new MethodParameter(method, pi);
		final NativeWebRequest request = mockery.mock(NativeWebRequest.class);
		@SuppressWarnings("unchecked")
		final PagingFactory<Object> pagingFactory = mockery.mock(PagingFactory.class);
		final Paging paging = mockery.mock(Paging.class);

		mockery.checking(new Expectations() {{
			if (resolved) {
				if (name == null) {
					oneOf(beanFactory).getBean(PagingFactory.class);
					will(returnValue(pagingFactory));
				}
				else {
					oneOf(beanFactory).getBean(name, PagingFactory.class);
					will(returnValue(pagingFactory));
				}

				oneOf(pagingFactory).getPaging(with(anything()));
				will(returnValue(paging));
			}

			if (store != null) {
				oneOf(storer).store(with(aNonNull(Paging.class)), with(equal(store)), with(aNonNull(NativeWebRequest.class)));
			}

			ignoring(request).getNativeRequest();
		}});

		final Object result = resolver.resolveArgument(param, request);

		if (resolved) {
			assertSame(paging, result);
		}
		else {
			assertSame(WebArgumentResolver.UNRESOLVED, result);
		}
	}

	private void doTestResolve(int cl, int mi, int pi, boolean resolve, String name) throws Exception {
		doTestResolve(cl, mi, pi, resolve, name, null);
	}

	private void doTestResolve(int cl, int mi, int pi, String name) throws Exception {
		doTestResolve(cl, mi, pi, true, name);
	}

	private void doTestResolve(int cl, int mi, String name) throws Exception {
		doTestResolve(cl, mi, 0, name);
	}

	private void doTestResolve(int cl, int mi, String name, String store) throws Exception {
		doTestResolve(cl, mi, 0, true, name, store);
	}


	private Method findMethod(Class<?> clazz, final String name) {
		final AtomicReference<Method> m = new AtomicReference<Method>();

		ReflectionUtils.doWithMethods(clazz, new MethodCallback() {
			@Override
			public void doWith(Method method) throws IllegalArgumentException, IllegalAccessException {
				m.set(method);
			}
		}, new MethodFilter() {
			@Override
			public boolean matches(Method method) {
				return method.getName().equals(name);
			}
		});

		return m.get();
	}

}
