package org.evolvis.spring.paging.impl;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.evolvis.spring.paging.Paging;
import org.junit.Before;
import org.junit.Test;

public class DefaultPagingTest {

	private DefaultPaging paging;

	@Before
	public void setup() {
		paging = new DefaultPaging();
	}


	@Test
	public void testCorrectPageAdaption() throws Exception {
		paging.setFirstPage(4);
		assertEquals(4, paging.getCurrentPage());

		paging.setCurrentPage(5);
		assertEquals(5, paging.getCurrentPage());

		paging.setCurrentPage(2);
		assertEquals(4, paging.getCurrentPage());

		paging.setCurrentPage(-3);
		assertEquals(4, paging.getCurrentPage());

		paging.setFirstPage(1);
		assertEquals(4, paging.getCurrentPage());

		paging.setCurrentPage(-3);
		assertEquals(1, paging.getCurrentPage());
	}

	@Test
	public void testEmpty() throws Exception {
		assertTrue(paging.isResultEmpty());
	}

	@Test
	public void testEmptyZero() throws Exception {
		paging.setTotalItems(0L);

		assertTrue(paging.isResultEmpty());
	}

	@Test
	public void testNotEmpty() throws Exception {
		paging.setTotalItems(1L);

		assertFalse(paging.isResultEmpty());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testInvalidTotal() throws Exception {
		paging.setTotalItems(-1L);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testInvalidIPPZero() throws Exception {
		paging.setItemsPerPage(0L);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testInvalidIPPNegative() throws Exception {
		paging.setItemsPerPage(-1L);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testNullPageParam() throws Exception {
		paging.setPageParameter(null);
	}


	@Test
	public void testEmptyPaging() throws Exception {
		paging.setFirstPage(1);
		paging.setFirstIndex(0L);
		paging.setItemsPerPage(10L);


		paging.setTotalItems(0L);
		paging.setCurrentPage(1);
		assertTrue(paging.isResultEmpty());
		assertTrue(paging.isTotalExceeded());
		try {
			paging.getLastPage();
			fail();
		} catch (IllegalStateException e) { }
		try {
			paging.getLastIndex();
			fail();
		} catch (IllegalStateException e) { }

		assertEquals(0L, paging.getStartIndex());
		assertEquals(10L, paging.getEndIndex());
	}


	@Test
	public void testCalculation() throws Exception {
		paging.setFirstPage(1);
		paging.setFirstIndex(0L);
		paging.setItemsPerPage(10L);


		paging.setTotalItems(33L);
		paging.setCurrentPage(1);
		check(paging,    0, 10, 32,   4);
		assertFalse(paging.isTotalExceeded());

		paging.setCurrentPage(2);
		check(paging,    10, 20, 32,   4);
		assertFalse(paging.isTotalExceeded());

		paging.setCurrentPage(3);
		check(paging,    20, 30, 32,   4);
		assertFalse(paging.isTotalExceeded());

		paging.setCurrentPage(4);
		check(paging,    30, 40, 32,   4);
		assertFalse(paging.isTotalExceeded());

		paging.setCurrentPage(5);
		check(paging,    40, 50, 32,   4);
		assertTrue(paging.isTotalExceeded());

		paging.setTotalItems(10L);
		paging.setCurrentPage(1);
		check(paging,    0, 10, 9,   1);
		assertFalse(paging.isTotalExceeded());

		paging.setCurrentPage(2);
		check(paging,    10, 20, 9,   1);
		assertTrue(paging.isTotalExceeded());

		paging.setTotalItems(4L);
		paging.setCurrentPage(1);
		check(paging,    0, 10, 3,   1);
		assertFalse(paging.isTotalExceeded());

		paging.setCurrentPage(2);
		check(paging,    10, 20, 3,   1);
		assertTrue(paging.isTotalExceeded());
	}


	private void check(Paging paging, long si, long ei, long li, long lp) {
		assertEquals(paging.getStartIndex(), paging.getOffset());
		assertEquals(paging.getItemsPerPage(), paging.getLimit());

		assertEquals(si, paging.getStartIndex());
		assertEquals(ei, paging.getEndIndex());
		assertEquals(li, paging.getLastIndex());

		assertEquals(lp, paging.getLastPage());
	}

}
