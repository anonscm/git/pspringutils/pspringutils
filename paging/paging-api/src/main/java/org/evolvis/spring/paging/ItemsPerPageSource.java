package org.evolvis.spring.paging;

/**
 * A source for the number of items per page.
 *
 * @param <T> The extraction resource type
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public interface ItemsPerPageSource<T> {

	/**
	 * Gets the number of items per page.
	 *
	 * @param source The extraction source
	 * @return The number of items per page or <code>null</code> if it cannot be determined
	 */
	public Long getItemsPerPage(T source);

}
