package org.evolvis.spring.paging;


/**
 * A factory for creating {@link Paging} objects.
 *
 * @param <T> The extraction resource type
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public interface PagingFactory<T> {

	/**
	 * Gets a paging object using the source for extraction.<br />
	 * <b>Note</b> that the {@link Paging#setTotalItems(long) totalItems} should be set afterwards!
	 *
	 * @param source The extraction source
	 * @return The paging object
	 * @see Paging#setTotalItems(long)
	 */
	public Paging getPaging(T source);

	/**
	 * Gets a paging object using the source for extraction.<br />
	 * The number of total items is also set if <code>totalDataItems</code> is not <code>null</code>.
	 *
	 * @param source The extraction source
	 * @param totalDataItems The number of total data items or <code>null</code> to leave it unset
	 * @return The paging object
	 */
	public Paging getPaging(T source, Long totalDataItems);

}
