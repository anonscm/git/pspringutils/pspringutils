package org.evolvis.spring.paging;

import java.io.Serializable;


/**
 * An object containing all relevant information for use with paging.
 *
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public interface Paging extends Serializable {

	/**
	 * Sets the number of total data items.
	 *
	 * @param total The new number of total items
	 * @throws IllegalArgumentException if value is negative
	 */
	public void setTotalItems(long total);


	/**
	 * Gets the number of total data items.
	 *
	 * @return The number of total items
	 */
	public long getTotalItems();

	/**
	 * Gets the first page number.
	 *
	 * @return The first page number
	 */
	public int getFirstPage();

	/**
	 * Gets the first index number.
	 *
	 * @return The first index number
	 */
	public long getFirstIndex();

	/**
	 * Gets the current page number.
	 *
	 * @return The current page number
	 */
	public int getCurrentPage();

	/**
	 * Gets the number of items per page.
	 *
	 * @return The number of items per page
	 */
	public long getItemsPerPage();

	/**
	 * Gets the page parameter.
	 *
	 * @return The page parameter
	 */
	public String getPageParameter();

	/**
	 * Checks if the total items are empty, meaning no items are present.
	 *
	 * @return true, if no items are present
	 */
	public boolean isResultEmpty();

	/**
	 * Gets the last page number.
	 *
	 * @return The last page number
	 * @throws IllegalStateException if total items are empty
	 */
	public int getLastPage();

	/**
	 * Gets the start item index (inclusive) usable for data retrieval.
	 *
	 * @return The start index
	 * @throws IllegalStateException if total items are empty
	 */
	public long getStartIndex();

	/**
	 * Gets the end item index (exclusive) usable for data retrieval.
	 *
	 * @return The end index
	 * @throws IllegalStateException if total items are empty
	 */
	public long getEndIndex();

	/**
	 * Gets the last item index possible regarding the number of total items.
	 *
	 * @return The last index
	 * @throws IllegalStateException if total items are empty
	 */
	public long getLastIndex();

	/**
	 * This is an alias for {@link #getStartIndex()}.
	 *
	 * @return The offset
	 */
	public long getOffset();

	/**
	 * This is an alias for {@link #getItemsPerPage()}.
	 *
	 * @return The limit
	 */
	public long getLimit();

	/**
	 * Checks if is total amount is exceeded by the current page.
	 *
	 * @return true, if is total amount is exceeded
	 */
	public boolean isTotalExceeded();

}
