package org.evolvis.spring.paging;

/**
 * A source for retrieving the current page number.
 *
 * @param <T> The extraction resource type
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public interface CurrentPageSource<T> {

	/**
	 * Gets the current page number.
	 *
	 * @param source The extraction source
	 * @return The current page number or <code>null</code> if it cannot be determined
	 */
	public Integer getCurrentPage(T source);

	/**
	 * Gets the page parameter name used for determining the current page number.
	 *
	 * @return The page parameter
	 */
	public String getPageParameter();

}
