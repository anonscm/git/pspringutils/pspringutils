package org.evolvis.spring.paging.sample;

import org.evolvis.spring.paging.Paging;
import org.evolvis.spring.paging.portlet.spring.PortletPagingHandlerInterceptor;
import org.evolvis.spring.paging.portlet.spring.PortletPagingWebArgumentResolver;
import org.evolvis.spring.paging.sample.service.SampleService;
import org.evolvis.spring.paging.spring.annotation.Pager;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * A sample controller to demonstrate the use of paging in the controller.
 *
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
@Controller
@RequestMapping("VIEW")
public class SampleController {

	/** The {@link SampleService service}. */
	private SampleService service;

	/**
	 * Sets the service.
	 *
	 * @param service The new service
	 */
	@Required
	public void setService(SampleService service) {
		this.service = service;
	}


	/**
	 * Shows the paging page completing and using the a {@link Paging} object.<br /><br />
	 * The {@link Paging} object is injected by the {@link PortletPagingWebArgumentResolver} and
	 * the {@link PortletPagingHandlerInterceptor} will publish the object in the view {@link Model model}
	 * using the &quot;paging&quot; attribute key.
	 *
	 * @param paging The injected paging object
	 * @param model The view model
	 * @return The view name
	 */
	@RenderMapping
	@Pager(model="paging")
	public String showPagingPage(Paging paging, Model model) {
		paging.setTotalItems(service.getTotalItems());

		model.addAttribute("items", service.getItems((int) paging.getOffset(), (int) paging.getLimit()));

		return "paging";
	}

}
