package org.evolvis.spring.paging.sample.service;

import static java.lang.Math.min;
import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

/**
 * A default {@link SampleService} implementation with a {@link List} repository backend.
 *
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
@Repository
public class DefaultSampleService implements SampleService {

	private static final int	ITEM_COUNT	= 124;
	private static final String	ITEM_PREFIX	= "Sample Item ";


	private List<String> repository = createSampleRepository();

	/**
	 * Sets the repository for this service.
	 */
	public void setRepository(List<String> repository) {
		this.repository = new ArrayList<String>(repository);
	}


	private static List<String> createSampleRepository() {
		List<String> repo = new ArrayList<String>(ITEM_COUNT);

		for (int i = 1; i <= ITEM_COUNT; i++) {
			repo.add(ITEM_PREFIX + i);
		}

		return repo;
	}


	@Override
	public int getTotalItems() {
		return repository.size();
	}

	@Override
	public List<String> getItems(int offset, int limit) {
		final int size = getTotalItems();

		// Prevent invalid indizes
		if (offset < 0 || offset >= size || limit <= 0) {
			return emptyList();
		}

		// Adjust queried limits to prevent indexes out of bounds
		final int startIndex = offset;
		final int endIndex = min(size, startIndex + limit);

		return unmodifiableList(repository.subList(startIndex, endIndex));
	}

}
