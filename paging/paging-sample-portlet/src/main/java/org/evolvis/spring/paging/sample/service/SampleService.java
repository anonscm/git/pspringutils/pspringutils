package org.evolvis.spring.paging.sample.service;

import java.util.List;


/**
 * A sample service to demonstrate a possible service backend.
 *
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public interface SampleService {

	/**
	 * Gets the total number of items available.
	 */
	public int getTotalItems();

	/**
	 * Gets the items in the specified range.
	 *
	 * @param offset The offset index from which to start
	 * @param limit The maximum number of items to retrieve
	 * @return The (possibly empty) list of items
	 */
	public List<String> getItems(int offset, int limit);

}
