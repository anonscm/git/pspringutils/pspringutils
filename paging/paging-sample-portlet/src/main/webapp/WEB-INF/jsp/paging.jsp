<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page" version="2.0"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:portlet="http://java.sun.com/portlet_2_0">
<h2>Welcome to the paging sample application</h2>

<c:choose>

<c:when test="${paging.resultEmpty}">
<div class="items nothing-found">No items have been found</div>
</c:when>

<c:otherwise>
<div class="paging paging-ipp">
<portlet:renderURL var="url" />
<form action="${url}" method="post">
Select the number of items per page: <select name="ipp">
<c:forEach begin="5" end="50" step="5" var="i">
<c:choose>
<c:when test="${paging.itemsPerPage eq i}">
	<option selected="selected">${i}</option>
</c:when>
<c:otherwise>
	<option>${i}</option>
</c:otherwise>
</c:choose>
</c:forEach>
</select> <button type="submit">Change</button>
</form></div>

<div class="paging">
<c:if test="${paging.currentPage gt paging.firstPage}">
	<portlet:renderURL var="url">
		<portlet:param name="ipp" value="${paging.itemsPerPage}"/>
		<portlet:param name="${paging.pageParameter}" value="${paging.firstPage}"/>
	</portlet:renderURL>
	<a href="${url}" title="Go to first page" style="margin-right:1em;font-size:larger">&lt;&lt;</a>
	
	<portlet:renderURL var="url">
		<portlet:param name="ipp" value="${paging.itemsPerPage}"/>
		<portlet:param name="${paging.pageParameter}" value="${paging.currentPage - 1}"/>
	</portlet:renderURL>
	<a href="${url}" title="Go to previous page" style="margin-right:1em;font-size:larger">&lt;</a>
</c:if>
<c:if test="${paging.currentPage lt paging.lastPage}">
	<portlet:renderURL var="url">
		<portlet:param name="ipp" value="${paging.itemsPerPage}"/>
		<portlet:param name="${paging.pageParameter}" value="${paging.currentPage + 1}"/>
	</portlet:renderURL>
	<a href="${url}" title="Go to next page" style="margin-right:1em;font-size:larger">&gt;</a>

	<portlet:renderURL var="url">
		<portlet:param name="ipp" value="${paging.itemsPerPage}"/>
		<portlet:param name="${paging.pageParameter}" value="${paging.lastPage}"/>
	</portlet:renderURL>
	<a href="${url}" title="Go to last page">&gt;&gt;</a>
</c:if>
</div>

<div class="paging paging-direct">
<portlet:renderURL var="url" />
<form action="${url}" method="post">
<input type="hidden" name="ipp" value="${paging.itemsPerPage}" />
Page <select name="${paging.pageParameter}">
<c:forEach begin="${paging.firstPage}" end="${paging.lastPage}" step="1" var="page">
<c:choose>
<c:when test="${paging.currentPage eq page}">
	<option selected="selected">${page}</option>
</c:when>
<c:otherwise>
	<option>${page}</option>
</c:otherwise>
</c:choose>
</c:forEach>
</select> of ${paging.lastPage} <button type="submit">Go</button>
</form></div>


<ul class="items">
<c:forEach items="${items}" var="item">
	<li><c:out value="${item}" /></li>
</c:forEach>
</ul>

</c:otherwise>
</c:choose>

</jsp:root>