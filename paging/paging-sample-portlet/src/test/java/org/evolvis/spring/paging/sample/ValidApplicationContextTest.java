package org.evolvis.spring.paging.sample;

import static org.junit.Assert.assertNotSame;

import org.evolvis.spring.paging.Paging;
import org.evolvis.spring.paging.portlet.PortletPagingFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.mock.web.portlet.MockRenderRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
	"file:src/main/webapp/WEB-INF/applicationContext.xml",
	"file:src/main/webapp/WEB-INF/samplepaging-portlet.xml"
})
public class ValidApplicationContextTest implements BeanFactoryAware {

	private BeanFactory beanFactory;

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}


	@Test
	public void testPagingBeanPrototypeScope() throws Exception {
		final Paging bean1 = beanFactory.getBean(Paging.class);
		final Paging bean2 = beanFactory.getBean(Paging.class);
		final Paging bean3 = beanFactory.getBean(Paging.class);

		assertNotSame(bean2, bean1);
		assertNotSame(bean3, bean1);
		assertNotSame(bean3, bean2);
	}

	@Test
	public void testPagingFactoryBeanPrototypeScope() throws Exception {
		final PortletPagingFactory pagingFactory = beanFactory.getBean(PortletPagingFactory.class);
		final MockRenderRequest request = new MockRenderRequest();

		final Paging bean1 = pagingFactory.getPaging(request);
		final Paging bean2 = pagingFactory.getPaging(request);
		final Paging bean3 = pagingFactory.getPaging(request);

		assertNotSame(bean2, bean1);
		assertNotSame(bean3, bean1);
		assertNotSame(bean3, bean2);
	}

}
