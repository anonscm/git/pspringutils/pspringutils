/**
 * portlet-spring-utils
 * Tarent's Portlet-Spring-Utils for Portlet development
 * Copyright (C) 2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * appended GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'portlet-spring-utils'
 * Signature of Elmar Geese, 2009
 * Elmar Geese, CEO tarent GmbH
 */

package org.evolvis.spring.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

/**
 * A simple FileServeController that serve files as resources. Use the 
 * filePatterns attribute to specify the valid resources as regex like 
 * '/img/[\w]{1,}\.[\w]{1,}'.
 * 
 * @author Tino Rink
 */
@Controller
@RequestMapping(value = {"EDIT", "VIEW", "HELP"})
public class FileServeController {
	
	private final Log logger = LogFactory.getLog(getClass());
	
	private final FileNameMap fileNameMap = URLConnection.getFileNameMap();

	private boolean usePortletContextResources = false;
	
	private List<Pattern> filePatterns = new ArrayList<Pattern>();

	/**
	 * Set one Regex used as FilePattern
	 */
	public void setFilePattern(String filePattern) {
		this.filePatterns.add(Pattern.compile(filePattern));
	}

	/**
	 * Set a List of Regex used as FilePatterns
	 */
	public void setFilePatterns(List<String> filePatterns) {
		for (String filePattern : filePatterns) {
			this.filePatterns.add(Pattern.compile(filePattern));
		}
	}

	/**
	 * Define that the PortletContext should be used to load Resources. Default is <b>false</b>.
	 */
	public void setUsePortletContextResources(boolean usePortletContextResources) {
		this.usePortletContextResources = usePortletContextResources;
	}
	
	@ResourceMapping
	public void serveFileResource(ResourceRequest request, ResourceResponse response) throws IOException {

		final String resourceId = StringUtils.cleanPath(request.getResourceID());
		
		if (logger.isDebugEnabled())		
			logger.debug("serveFileResource() called with resourceId=" + resourceId);
		
		if (resourceId == null)
			return;

		for (Pattern filePattern : filePatterns) {
			
			if (filePattern.matcher(resourceId).matches()) {
				
				final InputStream iStream;
				
				if (usePortletContextResources) {
					iStream = request.getPortletSession().getPortletContext().getResourceAsStream(resourceId);
				}
				else {
					iStream = getClass().getResourceAsStream(resourceId);
				}

				if (null != iStream) {
					response.setContentType(fileNameMap.getContentTypeFor(resourceId));
					FileCopyUtils.copy(iStream, response.getPortletOutputStream());
				}
				
				return;
			}
		}
	}
}
