/**
 * portlet-spring-utils
 * Tarent's Portlet-Spring-Utils for Portlet development
 * Copyright (C) 2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * appended GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'portlet-spring-utils'
 * Signature of Elmar Geese, 2009
 * Elmar Geese, CEO tarent GmbH
 */

package org.evolvis.spring.interceptor;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.handler.HandlerInterceptorAdapter;

/**
 * Spring-Portlet-Interceptor for ParameterMapping (Action -> Render -> ModelAndView ) with support of redirection / overwriting
 * for every transition. Redirection / overwriting is done via setting the corresponding Attribute on the Action- / RenderRequest.
 *
 * @author Tino Rink
 */
public class ParameterMappingInterceptor extends HandlerInterceptorAdapter {

    public static final String DEFAULT_PARAMETER_NAME = "action";

    private List<String> parameterList;

    public ParameterMappingInterceptor() {
    	parameterList = new ArrayList<String>(1);
    	parameterList.add(DEFAULT_PARAMETER_NAME);
	}

    /**
     * Set the name of the parameter used for mapping, default is "action".
     */
    public void setParameterName(String parameterName) {
        if (parameterName != null) {
        	parameterList = new ArrayList<String>();
        	parameterList.add(parameterName);
        }
    }

    /**
     * Set a list of the parameter names used for mapping, default is "action".
     */
    public void setParameterList(List<String> parameterList) {
        if (parameterList != null) {
        	this.parameterList = parameterList;
        }
    }

    @Override
    public void afterActionCompletion(final ActionRequest request,final ActionResponse response,final Object handler,final Exception ex) throws Exception {
        // copy parameter value
        try {

        	for (final String parameterName : this.parameterList) {

	            // check for redirection / overwriting via attribute
	            final Object attributeValue = request.getAttribute(parameterName);
	            if (null != attributeValue) {
	                response.setRenderParameter(parameterName, String.valueOf(attributeValue));
	                continue;
	            }

	            // use request parameter if available
	            final String parameterValue = request.getParameter(parameterName);
	            if (null != parameterValue) {
	                response.setRenderParameter(parameterName, parameterValue);
	                continue;
	            }
        	}
        }
        catch (IllegalStateException ise) {
            // probably sendRedirect called... no need to handle this exception
        }
    }

    @Override
    public void postHandleRender(final RenderRequest request, final RenderResponse response, final Object handler, final ModelAndView modelAndView)
            throws Exception {
    	if (modelAndView == null) {
    		throw new IllegalStateException("No model received from handler, use or return a Model in your handler");
    	}

    	for (final String parameterName : this.parameterList) {

	        // check for redirection / overwriting via attribute
	        final Object attributeValue = request.getAttribute(parameterName);
	        if (null != attributeValue) {
	            modelAndView.addObject(parameterName, attributeValue);
	            continue;
	        }

	        // use request parameter if available
	        final String parameterValue = request.getParameter(parameterName);
	        if (null != parameterValue) {
	            modelAndView.addObject(parameterName, parameterValue);
	            continue;
	        }
    	}
    }
}
