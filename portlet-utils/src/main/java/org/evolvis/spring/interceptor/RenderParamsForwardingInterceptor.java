/**
 * portlet-spring-utils
 * Tarent's Portlet-Spring-Utils for Portlet development
 * Copyright (C) 2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * appended GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'portlet-spring-utils'
 * Signature of Elmar Geese, 2009
 * Elmar Geese, CEO tarent GmbH
 */

package org.evolvis.spring.interceptor;

import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static org.springframework.util.StringUtils.delimitedListToStringArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletRequest;
import javax.portlet.StateAwareResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.portlet.HandlerInterceptor;
import org.springframework.web.portlet.handler.HandlerInterceptorAdapter;

/**
 * A {@link HandlerInterceptor} for passing parameters from action and event phases to the render phase.
 *
 * @author Daniel Ball &lt;d.ball@tarent.de&gt;
 */
public class RenderParamsForwardingInterceptor extends HandlerInterceptorAdapter {

	private final Log log = LogFactory.getLog(getClass());

	private Collection<String> includeParams;
	private Collection<String> excludeParams;

	private Collection<Object> includeHandlers;
	private Collection<Object> excludeHandlers;

	private boolean processOnExceptions = true;


	/**
	 * Sets the parameter names to include separated by spaces.
	 *
	 * @param params The parameter names
	 */
	public void setIncludeParams(String params) {
		setIncludeParams(params, " ");
	}

	/**
	 * Sets the parameter names to include separated by delimiter.
	 *
	 * @param params The parameter names
	 * @param delimiter The name delimiter
	 */
	public void setIncludeParams(String params, String delimiter) {
		setIncludeParams(asList(delimitedListToStringArray(params, delimiter)));
	}

	/**
	 * Sets the parameter names to include.
	 *
	 * @param params The parameter names
	 */
	public void setIncludeParams(Collection<String> params) {
		this.includeParams = params;
	}

	/**
	 * Sets the parameter names to exclude separated by spaces.
	 *
	 * @param params The parameter names
	 */
	public void setExcludeParams(String params) {
		setExcludeParams(params, " ");
	}

	/**
	 * Sets the parameter names to exclude separated by delimiter.
	 *
	 * @param params The parameter names
	 * @param delimiter The name delimiter
	 */
	public void setExcludeParams(String params, String delimiter) {
		setExcludeParams(asList(delimitedListToStringArray(params, delimiter)));
	}

	/**
	 * Sets the parameter names to exclude.
	 *
	 * @param params The parameter names
	 */
	public void setExcludeParams(Collection<String> params) {
		this.excludeParams = params;
	}


	/**
	 * Sets the handler for which to apply this interceptor only.
	 *
	 * @param handler The handler
	 */
	public void setHandler(Object handler) {
		setIncludeHandlers(singleton(handler));
	}

	/**
	 * Sets the handlers for which to apply this interceptor only.
	 *
	 * @param handlers The handlers
	 */
	public void setIncludeHandlers(Collection<Object> handlers) {
		this.includeHandlers = handlers;
	}

	/**
	 * Sets the handlers for which this interceptor must <b>NOT</b> be applied.
	 *
	 * @param handlers The handlers
	 */
	public void setExcludeHandlers(Collection<Object> handlers) {
		this.excludeHandlers = handlers;
	}


	/**
	 * Set to <code>true</code> if the parameters shall be copied even
	 * if an exception was thrown in the handler, defaults to <code>true</code>.
	 *
	 * @param processOnExceptions <code>True</code> to copy parameters also on failed handlers
	 */
	public void setProcessOnExceptions(boolean processOnExceptions) {
		this.processOnExceptions = processOnExceptions;
	}



	/**
	 * {@inheritDoc}
	 * @see #processCompletion(PortletRequest, StateAwareResponse, Object, Exception)
	 */
	@Override
	public void afterActionCompletion(ActionRequest request, ActionResponse response, Object handler, Exception ex)
	throws Exception {
		processCompletion(request, response, handler, ex);
	}

	/**
	 * {@inheritDoc}
	 * @see #processCompletion(PortletRequest, StateAwareResponse, Object, Exception)
	 */
	@Override
	public void afterEventCompletion(EventRequest request, EventResponse response, Object handler, Exception ex)
	throws Exception {
		processCompletion(request, response, handler, ex);
	}



	/**
	 * Processes a phase completion by copying the parameters into the render phase.
	 *
	 * @param request The portlet request
	 * @param response The portlet response
	 * @param handler The handler
	 * @param ex The exception thrown
	 */
	private void processCompletion(PortletRequest request, StateAwareResponse response, Object handler, Exception ex) {
		if (!processOnExceptions && ex != null || !isHandledHandler(handler)) {
			return;
		}


		Collection<String> params = collectParameters(request);
		Map<String, String[]> availableParams = response.getRenderParameterMap();

		if (log.isDebugEnabled()) log.debug("Processing parameters: "+params);

		try {
			for (final String paramName : params) {
				// Just set render param if not already set
				if (!availableParams.containsKey(paramName)) {
					final String[] paramValues = request.getParameterValues(paramName);

					response.setRenderParameter(paramName, paramValues);

					if (log.isTraceEnabled()) {
						log.trace("Set render parameter "+paramName+" with "+paramValues.length+" values to "+Arrays.toString(paramValues));
					}
				}
			}
		}
		catch (IllegalStateException e) {
			log.debug("Skipped render parameters due to redirect");
		}
	}


	/**
	 * Checks if the handler is to be handled by this interceptor.
	 *
	 * @param handler The handler
	 * @return true, if the handler is handled
	 */
	private boolean isHandledHandler(final Object handler) {
		return ((excludeHandlers == null || !excludeHandlers.contains(handler)) &&
				(includeHandlers == null || includeHandlers.contains(handler)));
	}

	/**
	 * Collects the request parameters and returns them filtered.
	 *
	 * @param request The portlet request
	 * @return The filtered parameters
	 */
	private Collection<String> collectParameters(PortletRequest request) {
		final ArrayList<String> params = Collections.list(request.getParameterNames());

		if (excludeParams != null) {
			params.removeAll(excludeParams);
		}

		if (includeParams != null) {
			params.retainAll(includeParams);
		}

		return params;
	}

}
