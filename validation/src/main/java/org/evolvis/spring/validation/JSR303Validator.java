/**
 * portlet-spring-utils
 * Tarent's Portlet-Spring-Utils for Portlet development
 * Copyright (C) 2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * appended GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'portlet-spring-utils'
 * Signature of Elmar Geese, 2009
 * Elmar Geese, CEO tarent GmbH
 */

package org.evolvis.spring.validation;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.metadata.BeanDescriptor;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;

/**
 * This is a generic spring validator implementation which delegates the
 * validation tasks to the JSR-303 validator. It validates recursive the target object
 * with all its properties.
 * <br>
 * Using the <code>recursive</code> property you can control the behavior of recursion.
 * Default <code>recursive</code> is set to true.
 * <br>
 * Default constrain and argument definitions:<br>
 * - DecimalMax -> {value}<br>
 * - DecimalMin -> {value}<br>
 * - Digits.class -> {integer, fraction}<br>
 * - Max -> {value}<br>
 * - Min -> {value}<br>
 * - Pattern -> {regexp}<br>
 * - Size -> {min, max}<br>
 *
 * @author Tino Rink
 * @author Dmytro Mayster
 */
public class JSR303Validator implements org.springframework.validation.Validator {

	private static final Validator validator;
	static {
		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.usingContext().getValidator();
	}

    private boolean addPropertyPathToFieldError = false;

    /**
     * Define if the field name should be appand to the error code, e.g. "violation.notNull" or "violation.notNull.fieldName".<br>
     * Default is <b>false</b>
     */
    public void setAddPropertyPathToFieldError(boolean addPropertyPathToFieldError) {
        this.addPropertyPathToFieldError = addPropertyPathToFieldError;
    }

	private boolean recursive = true;

    /**
     * Define if the fields should be validated recursively.<br>
     * Default is <b>true</b>
     */
	public void setRecursive(boolean recursive) {
		this.recursive = recursive;
	}

	private final Map<Class<?>, String[]> constraintArguments = new HashMap<Class<?>, String[]>();

	/**
	 * Defines the properties of a condition that values will be passed as arguments in case of an violation.
	 */
	public void setConstraintArguments(Class<?> constraint, String... argNames) {
		constraintArguments.put(constraint, argNames);
	}

	/**
	 * Defines the properties of a condition that values will be passed as arguments in case of an violation.
	 */
	public void setConstraintArguments(String constrainName, String... argNames) throws ClassNotFoundException {
		setConstraintArguments(Class.forName(constrainName), argNames);
	}

	/**
	 * Defines the properties of a condition that values will be passed as arguments in case of an violation.
	 */
	public void setConstraintArguments(Class<?> constraint, List<String> argNames) {
		constraintArguments.put(constraint, argNames.toArray(new String[0]));
	}

	/**
	 * Defines the properties of a condition that values will be passed as arguments in case of an violation.
	 */
	public void setConstraintArguments(String constrainName, List<String> argNames) throws ClassNotFoundException {
		setConstraintArguments(Class.forName(constrainName), argNames.toArray(new String[0]));
	}

	boolean oneViolationPerField = false;

	/**
	 * Defines the only one violation per field should be returned.<br>
	 * Default is <b>false</b>.
	 */
	public void setOneViolationPerField(boolean oneFieldViolation) {
		this.oneViolationPerField = oneFieldViolation;
	}

	private boolean genericErrorCodes = false;

	/**
	 * Defines that generic error codes should be created.<br>
	 * Default is <b>false</b>.
	 */
	public void setGenericErrorCodes(boolean genericErrorCodes) {
		this.genericErrorCodes = genericErrorCodes;
	}

	public JSR303Validator() {
		setConstraintArguments(DecimalMax.class, "value");
		setConstraintArguments(DecimalMin.class, "value");
		setConstraintArguments(Digits.class, "integer", "fraction");
		setConstraintArguments(Max.class, "value");
		setConstraintArguments(Min.class, "value");
		setConstraintArguments(Pattern.class, "regexp");
		setConstraintArguments(Size.class, "min", "max");
	}

	@Override
	public boolean supports(final Class<?> clazz) {
		final BeanDescriptor beanDescriptor = validator.getConstraintsForClass(clazz);
		return beanDescriptor.hasConstraints() || !beanDescriptor.getConstrainedProperties().isEmpty();
	}

	@Override
	public void validate(final Object target, final Errors errors) {

		final Set<ConstraintViolation<Object>> constraintViolations = validator.validate(target);

		for (final ConstraintViolation<Object> violation : constraintViolations) {

			final Class<?> annotation = violation.getConstraintDescriptor().getAnnotation().annotationType();
			final String[] constrainArguments = this.constraintArguments.get(annotation);
			final Object[] constrainAttributeValues;

			if (constrainArguments == null || constrainArguments.length == 0) {
				constrainAttributeValues = null;
			}
			else {

				final Map<String, Object> attributeMap = violation.getConstraintDescriptor().getAttributes();
				final List<Object> attributeValues = new ArrayList<Object>();

				for (String constrainAttribute : constrainArguments) {
					final Object constrainAttributeValue = attributeMap.get(constrainAttribute);
					if (constrainAttributeValue != null) {
						attributeValues.add(constrainAttributeValue);
					}
				}

				constrainAttributeValues = attributeValues.toArray();
			}

			if (null == violation.getPropertyPath() || "".equals(violation.getPropertyPath().toString())) {
				errors.reject(violation.getMessage(), constrainAttributeValues, null);
			}
			else {

				final StringBuilder errorCode = new StringBuilder();

				if (genericErrorCodes) {
					errorCode.append("violation.");
					errorCode.append(StringUtils.uncapitalize(annotation.getSimpleName()));
				}
				else {
					errorCode.append(violation.getMessage());
				}

                if (addPropertyPathToFieldError) {
                	errorCode.append(".").append(violation.getPropertyPath());
                }

                errors.rejectValue(violation.getPropertyPath().toString(), errorCode.toString(), constrainAttributeValues, null);
			}

			if (oneViolationPerField) break;
		}

		if (recursive) {

			// It validated all properties from the target
			final BeanWrapper beanWrapper = new BeanWrapperImpl(target);

			for (PropertyDescriptor propertyDescriptor : beanWrapper.getPropertyDescriptors()) {

				final Object propertyValue = beanWrapper.getPropertyValue(propertyDescriptor.getName());

				if (propertyValue != null && supports(propertyValue.getClass())) {
					errors.pushNestedPath(propertyDescriptor.getName());
					validate(propertyValue, errors);
					errors.popNestedPath();
				}
			}
		}
	}
}
