/**
 * portlet-spring-utils
 * Tarent's Portlet-Spring-Utils for Portlet development
 * Copyright (C) 2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * appended GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'portlet-spring-utils'
 * Signature of Elmar Geese, 2009
 * Elmar Geese, CEO tarent GmbH
 */

package org.evolvis.spring.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * The named fields of the annotated class must all be equal.
 *
 * @author Tino Rink
 */
@Retention(RUNTIME)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Constraint(validatedBy = {FieldEquality.Validator.class})
public @interface FieldEquality {

    /**
     * Names of the fields that should be validated for equality.
     */
    String[] value();

    Class<?>[] payload() default {};

    /**
     * Default message
     */
    String message() default "violation.fieldEquality";


    Class<?>[] groups() default {};

    /**
     * Validator for FieldEquality
     */
    class Validator implements ConstraintValidator<FieldEquality, Object> {

        private String[] targetFieldNames;


        public void initialize(FieldEquality constraintAnnotation) {
            targetFieldNames = constraintAnnotation.value();
        }


        public boolean isValid(Object value, ConstraintValidatorContext context) {

            if (null == value) {
                return true;
            }

            final ArrayList<Field> targetFields = new ArrayList<Field>();

            final Field[] declaredFields = value.getClass().getDeclaredFields();

            for (final String targetFieldName : targetFieldNames) {

                for (final Field declaredField : declaredFields) {

                    if (targetFieldName.equals(declaredField.getName())) {
                        declaredField.setAccessible(true);
                        targetFields.add(declaredField);
                    }
                }
            }

            if (targetFields.isEmpty()) {
                return true;
            }

            final Iterator<Field> fieldIterator = targetFields.iterator();
            try {

                final Field firstField = fieldIterator.next();
                final Object firstValue = firstField.get(value);

                while (fieldIterator.hasNext()) {

                    final Field nextField = fieldIterator.next();
                    final Object nextValue = nextField.get(value);

                    if (firstValue == null ? firstValue != nextValue : !firstValue.equals(nextValue)) {
                        return false;
                    }
                }

                return true;
            }
            catch (Exception e) {
                // shouldn't happen
                throw new RuntimeException(e);
            }
        }
    }
}
