/**
 * portlet-spring-utils
 * Tarent's Portlet-Spring-Utils for Portlet development
 * Copyright (C) 2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * appended GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'portlet-spring-utils'
 * Signature of Elmar Geese, 2009
 * Elmar Geese, CEO tarent GmbH
 */

package org.evolvis.spring.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.Size;

/**
 * @author Tino Rink
 */
@Retention(RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
@Size(message = "violation.notEmpty", min = 1)
@Constraint(validatedBy = {NotEmpty.Validator.class})
public @interface NotEmpty {

    String message() default "";

    Class<?>[] payload() default {};

    Class<?>[] groups() default {};

    static class Validator implements ConstraintValidator<NotEmpty, String> {

        public void initialize(NotEmpty constraintAnnotation) {}

        public boolean isValid(String value, ConstraintValidatorContext context) {
            return true;
        }
    }
}
