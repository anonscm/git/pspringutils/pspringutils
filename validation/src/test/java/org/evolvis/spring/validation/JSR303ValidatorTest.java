package org.evolvis.spring.validation;
/**
 * portlet-spring-utils
 * Tarent's Portlet-Spring-Utils for Portlet development
 * Copyright (C) 2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * appended GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'portlet-spring-utils'
 * Signature of Elmar Geese, 2009
 * Elmar Geese, CEO tarent GmbH
 */

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.DirectFieldBindingResult;

public class JSR303ValidatorTest {

	private static class User {

		@NotNull
		@NotEmpty
		@Size(message = "violation.size", min = 2, max = 4)
		@Pattern(regexp = "^\\w+$")
		private String name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			User other = (User) obj;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "User [name=" + name + "]";
		}
	}


	private JSR303Validator jsr303Validator;

	@Before
	public void setup() {
		jsr303Validator = new JSR303Validator();
		jsr303Validator.setGenericErrorCodes(true);
	}

	@Test
	public void testUserNameNull() throws Exception {
		User user = new User();
		user.setName(null);

		DirectFieldBindingResult errors = new DirectFieldBindingResult(user, "user");

		jsr303Validator.validate(user, errors);

		assertTrue(errors.hasErrors());
		assertNull(user.getName());
		assertEquals(1, errors.getErrorCount());
		assertEquals(1, errors.getFieldErrorCount("name"));
	}

	@Test
	public void testEmptyUserName() throws Exception {
		User user = new User();
		user.setName("");

		DirectFieldBindingResult errors = new DirectFieldBindingResult(user, "user");

		jsr303Validator.validate(user, errors);

		assertTrue(errors.hasErrors());
		assertEquals("", user.getName());
		assertEquals(3, errors.getErrorCount());
		assertEquals(3, errors.getFieldErrorCount("name"));
	}

	@Test
	public void testUserName() throws Exception {
		final String name = "aBc";

		User user = new User();
		user.setName(name);

		DirectFieldBindingResult errors = new DirectFieldBindingResult(user, "user");

		jsr303Validator.validate(user, errors);

		assertFalse(errors.hasErrors());
		assertEquals(name, user.getName());
		assertEquals(0, errors.getErrorCount());
		assertEquals(0, errors.getFieldErrorCount("name"));
	}

}
